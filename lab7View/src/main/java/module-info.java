module com.example.lab7view {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.lab7view to javafx.fxml;
    exports com.example.lab7view;
}