package services.authdecorator;

import db.Database;
import entities.Project;
import entities.Stage;
import entities.Task;
import entities.User;


public class TaskToNextStageDecorator extends AccessTaskDataDecorator {
    public TaskToNextStageDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Task taskToMove = getCurrentTask();
        Stage taskStage = taskToMove.getCurrentStage();
        int indexOfCurrentStage = getCurrentProject().getStages().indexOf(taskStage);
        taskToMove.setCurrentStage(getCurrentProject().getStages().get(indexOfCurrentStage + 1));
        Database.getInstance().getDatastore().save(taskToMove);
        return currentUser;
    }
}
