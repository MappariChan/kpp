package services.authdecorator;

import db.Database;
import entities.Project;
import entities.Stage;
import entities.Task;
import entities.User;

public class AddTaskDecorator extends AccessProjectDataDecorator{
    private Task taskToAdd;
    private String stageName;
    public AddTaskDecorator(IAuth wrappee,String stageName, Task taskToAdd){
        super(wrappee);
        this.stageName = stageName;
        this.taskToAdd = taskToAdd;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Project currentProject = getCurrentProject();
        taskToAdd.setCurrentStage(currentProject.getStages().stream().filter(stage -> stage.getName().equals(stageName)).findFirst().get());
        Database.getInstance().getDatastore().save(taskToAdd);
        currentProject.getTasks().add(taskToAdd);
        Database.getInstance().getDatastore().save(getCurrentProject());
        return currentUser;
    }
}
