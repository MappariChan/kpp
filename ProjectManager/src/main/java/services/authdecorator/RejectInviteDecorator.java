package services.authdecorator;

import db.Database;
import entities.Invite;
import entities.Project;
import entities.User;

public class RejectInviteDecorator extends AccessDataDecorator{
    int inviteIndex;
    public RejectInviteDecorator(IAuth wrappee, int inviteIndex){
        super(wrappee);
        this.inviteIndex = inviteIndex;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Invite invite = currentUser.getInvites().get(inviteIndex);
        currentUser.getInvites().remove(inviteIndex);
        Database.getInstance().getDatastore().delete(invite);
        Database.getInstance().getDatastore().save(currentUser);
        return currentUser;
    }
}
