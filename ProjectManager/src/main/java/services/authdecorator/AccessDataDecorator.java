package services.authdecorator;

import entities.User;

public class AccessDataDecorator implements IAuth {
    private IAuth wrappee;
    public AccessDataDecorator(IAuth wrappee){
        this.wrappee = wrappee;
    }
    @Override
    public User execute(User user){
        return wrappee.execute(user);
    }
}
