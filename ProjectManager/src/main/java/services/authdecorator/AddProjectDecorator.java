package services.authdecorator;

import db.Database;
import entities.Project;
import entities.User;

public class AddProjectDecorator extends AccessDataDecorator {
    private Project projectToAdd;

    public Project getProjectToAdd() {
        return projectToAdd;
    }

    public void setProjectToAdd(Project projectToAdd) {
        this.projectToAdd = projectToAdd;
    }

    public AddProjectDecorator(IAuth wrappee, Project projectToAdd){
        super(wrappee);
        this.projectToAdd = projectToAdd;
    }

    @Override
    public User execute(User user){
        User currentUser = super.execute(user);
        projectToAdd.setOwner(currentUser);
        Database.getInstance().getDatastore().save(projectToAdd);
        currentUser.getProjectList().add(projectToAdd);
        Database.getInstance().getDatastore().save(currentUser);
        return currentUser;
    }
}
