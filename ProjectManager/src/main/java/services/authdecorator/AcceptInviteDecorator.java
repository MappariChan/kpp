package services.authdecorator;

import db.Database;
import entities.Invite;
import entities.Project;
import entities.User;

public class AcceptInviteDecorator extends AccessDataDecorator{
    int inviteIndex;
    public AcceptInviteDecorator(IAuth wrappee, int inviteIndex){
        super(wrappee);
        this.inviteIndex = inviteIndex;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Invite invite = currentUser.getInvites().get(inviteIndex);
        Project inviteProject = invite.getTo();
        currentUser.getInvites().remove(inviteIndex);
        Database.getInstance().getDatastore().delete(invite);
        currentUser.getProjectList().add(inviteProject);
        Database.getInstance().getDatastore().save(currentUser);
        return currentUser;
    }
}
