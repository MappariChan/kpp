package services.authdecorator;

import entities.Project;
import entities.User;

import java.util.List;

public class GetProjectsDecorator extends AccessDataDecorator{
    List<Project> projects;
    public GetProjectsDecorator(IAuth wrappee){
        super(wrappee);
    }

    public List<Project> getProjects() {
        return projects;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        projects = currentUser.getProjectList();
        return currentUser;
    }
}
