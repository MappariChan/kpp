package services.authdecorator;

import entities.Project;
import entities.Stage;
import entities.User;

import java.util.List;

public class GetStagesDecorator extends AccessProjectDataDecorator {
    private List<Stage> stages;

    public List<Stage> getStages() {
        return stages;
    }

    public GetStagesDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Project currentProject = getCurrentProject();
        stages = currentProject.getStages();
        return currentUser;
    }
}
