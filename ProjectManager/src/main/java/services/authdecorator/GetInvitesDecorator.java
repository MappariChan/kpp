package services.authdecorator;

import entities.Invite;
import entities.User;

import java.util.List;

public class GetInvitesDecorator extends AccessDataDecorator{
    private List<Invite> invites;

    public List<Invite> getInvites() {
        return invites;
    }

    public GetInvitesDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        invites = currentUser.getInvites();
        return currentUser;
    }
}
