package services.authdecorator;

import entities.User;

public interface IAuth {
    User execute(User user);
}
