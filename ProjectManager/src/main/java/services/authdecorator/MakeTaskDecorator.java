package services.authdecorator;

import db.Database;
import entities.SubTask;
import entities.Task;
import entities.User;

import java.util.Date;

public class MakeTaskDecorator extends AccessTaskDataDecorator {
    public MakeTaskDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        getCurrentTask().setDone(true);
        getCurrentTask().setDateEnd(new Date());
        for(SubTask subTask : getCurrentTask().getSubTasks()){
            subTask.setDone(true);
            Database.getInstance().getDatastore().save(subTask);
        }
        Database.getInstance().getDatastore().save(getCurrentTask());
        return currentUser;
    }
}
