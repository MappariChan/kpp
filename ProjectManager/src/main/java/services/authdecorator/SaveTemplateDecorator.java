package services.authdecorator;

import db.Database;
import entities.Template;
import entities.User;

public class SaveTemplateDecorator extends AccessProjectDataDecorator{
    private Template templateToSave;
    public SaveTemplateDecorator(IAuth wrappee, Template templateToSave){
        super(wrappee);
        this.templateToSave = templateToSave;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        templateToSave.setStages(getCurrentProject().getStages());
        Database.getInstance().getDatastore().save(templateToSave);
        currentUser.getTemplates().add(templateToSave);
        Database.getInstance().getDatastore().save(currentUser);
        return currentUser;
    }
}
