package services.authdecorator;

import db.Database;
import entities.SubTask;
import entities.User;

public class AddSubTaskDecorator extends AccessTaskDataDecorator{
    SubTask subTaskToAdd;

    public AddSubTaskDecorator(IAuth wrappee, SubTask subTask){
        super(wrappee);
        subTaskToAdd = subTask;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        getCurrentTask().getSubTasks().add(subTaskToAdd);
        Database.getInstance().getDatastore().save(getCurrentTask());
        return currentUser;
    }
}
