package services.authdecorator;

import entities.Task;
import entities.User;

import java.util.List;

public class GetTasksDecorator extends AccessProjectDataDecorator{
    private List<Task> tasks;

    public List<Task> getTasks() {
        return tasks;
    }

    public GetTasksDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        tasks = getCurrentProject().getTasks();
        return currentUser;
    }
}
