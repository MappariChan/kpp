package services.authdecorator;

import db.Database;
import entities.Project;
import entities.User;
import io.jsonwebtoken.Jwts;
import org.mongodb.morphia.query.Query;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AccessProjectDataDecorator extends AccessDataDecorator {
    private Project currentProject;
    public AccessProjectDataDecorator(IAuth wrappee){
        super(wrappee);
    }

    public Project getCurrentProject() {
        return currentProject;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        currentProject = findCurrentProject(currentUser);
        return currentUser;
    }

    private Project findCurrentProject(User user){
        String fileName = "C:\\localstorage\\project.txt"; // Specify the file name

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String projectName = reader.readLine();
            return user.getProjectList().stream().filter(project -> project.getName().equals(projectName)).findFirst().get();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }
}
