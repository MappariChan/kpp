package services.authdecorator;

import db.Database;
import entities.User;
import io.jsonwebtoken.Jwts;
import main.Main;
import org.mongodb.morphia.query.Query;

import java.io.*;

public class Auth implements IAuth{
    @Override
    public User execute(User user){
        return findCurrentUser();
    }

    private User findCurrentUser(){
        String fileName = "C:\\localstorage\\token.txt"; // Specify the file name

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String token = reader.readLine();
            String email = Jwts.parser().
                    setSigningKey("my-secret-key"). // Replace with your actual secret key .
                    parseClaimsJws(token).
                    getBody().getSubject();
            Query<User> query = Database.
                    getInstance().
                    getDatastore().
                    createQuery(User.class);
            query.criteria("email").equal(email);
            return query.get();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }
}
