package services.authdecorator;

import db.Database;
import entities.SubTask;
import entities.Task;
import entities.User;

import java.util.Date;

public class MakeSubTaskDecorator extends AccessTaskDataDecorator {
    private String subTaskTitle;

    public MakeSubTaskDecorator(IAuth wrappee, String subTaskTitle){
        super(wrappee);
        this.subTaskTitle = subTaskTitle;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Task currentTask = getCurrentTask();
        SubTask subTask = currentTask.getSubTasks().stream().filter(task -> task.getTitle().equals(subTaskTitle)).findFirst().get();
        subTask.setDone(true);
        Database.getInstance().getDatastore().save(subTask);
        boolean isTaskDone = true;
        for(SubTask task : currentTask.getSubTasks()) {
            if (!task.isDone()) {
                isTaskDone = false;
                break;
            }
        }
        if(isTaskDone){
            getCurrentTask().setDone(isTaskDone);
            getCurrentTask().setDateEnd(new Date());
            Database.getInstance().getDatastore().save(getCurrentTask());
        }
        return currentUser;
    }
}
