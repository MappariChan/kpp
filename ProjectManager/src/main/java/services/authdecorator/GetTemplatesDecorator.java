package services.authdecorator;

import entities.Template;
import entities.User;

import java.util.List;

public class GetTemplatesDecorator extends AccessDataDecorator{
    private List<Template> templates;

    public List<Template> getTemplates() {
        return templates;
    }

    public GetTemplatesDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        templates = currentUser.getTemplates();
        return currentUser;
    }
}
