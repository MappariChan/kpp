package services.authdecorator;

import db.Database;
import entities.Invite;
import entities.User;

public class InviteUserDecorator extends AccessProjectDataDecorator {
    private User userToInvite;

    public User getUserToInvite() {
        return userToInvite;
    }

    public InviteUserDecorator(IAuth wrappee, User userToInvite){
        super(wrappee);
        this.userToInvite = userToInvite;
    }
    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        if(getCurrentProject().getOwner() == currentUser) {
            Invite invite = new Invite();
            invite.setFrom(currentUser);
            invite.setTo(getCurrentProject());
            Database.getInstance().getDatastore().save(invite);
            userToInvite.getInvites().add(invite);
            Database.getInstance().getDatastore().save(userToInvite);
        }
        return currentUser;
    }
}
