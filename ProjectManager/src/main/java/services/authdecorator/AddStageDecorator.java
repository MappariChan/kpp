package services.authdecorator;

import db.Database;
import entities.Project;
import entities.Stage;
import entities.User;

public class AddStageDecorator extends AccessProjectDataDecorator {
    private Stage stageToAdd;
    private int stageOrderNumber;
    public AddStageDecorator(IAuth wrappee, Stage stageToAdd, int stageOrderNumber){
        super(wrappee);
        this.stageToAdd = stageToAdd;
        this.stageOrderNumber = stageOrderNumber;
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        Project currentProject = getCurrentProject();
        currentProject.getStages().add(stageOrderNumber, stageToAdd);
        Database.getInstance().getDatastore().save(currentProject);
        return currentUser;
    }
}
