package services.authdecorator;

import entities.Project;
import entities.Task;
import entities.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class AccessTaskDataDecorator extends AccessProjectDataDecorator{
    private Task currentTask;

    public Task getCurrentTask() {
        return currentTask;
    }

    public AccessTaskDataDecorator(IAuth wrappee){
        super(wrappee);
    }

    @Override
    public User execute(User user) {
        User currentUser = super.execute(user);
        currentTask = findCurrentTask();
        return currentUser;
    }

    private Task findCurrentTask(){
        String fileName = "C:\\localstorage\\task.txt"; // Specify the file name

        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String taskTitle = reader.readLine();
            return getCurrentProject().getTasks().stream().filter(task -> task.getTitle().equals(taskTitle)).findFirst().get();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return null;
    }
}
