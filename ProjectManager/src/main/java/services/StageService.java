package services;

import db.Database;
import entities.Project;
import entities.Stage;
import services.authdecorator.*;

import java.util.List;

public class StageService {
    public static Stage addStage(String stageName, int stageOrderNumber){
        Stage stageToAdd = new Stage();
        stageToAdd.setName(stageName);
        Database.getInstance().getDatastore().save(stageToAdd);
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth addStageMiddleware = new AddStageDecorator(currentProjectMiddleware, stageToAdd, stageOrderNumber);
        addStageMiddleware.execute(null);
        return stageToAdd;
    }

    public static List<Stage> getStages(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth getStagesMiddleware = new GetStagesDecorator(currentProjectMiddleware);
        getStagesMiddleware.execute(null);
        return  ((GetStagesDecorator)getStagesMiddleware).getStages();
    }
}
