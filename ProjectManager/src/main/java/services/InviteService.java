package services;

import db.Database;
import entities.Invite;
import entities.User;
import org.mongodb.morphia.query.Query;
import services.authdecorator.*;

import java.util.List;

public class InviteService {
    public static List<Invite> getInvites(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth getInvitesMiddleware = new GetInvitesDecorator(authMiddleware);
        getInvitesMiddleware.execute(null);
        return ((GetInvitesDecorator)getInvitesMiddleware).getInvites();
    }

    public static User inviteUser(String name){
        Query<User> userToInviteQuery = Database.getInstance().getDatastore().createQuery(User.class);
        userToInviteQuery.or(
                userToInviteQuery.criteria("login").equal(name),
                userToInviteQuery.criteria("email").equal(name)
        );
        User userToInvite = userToInviteQuery.get();
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth inviteUserMiddleware = new InviteUserDecorator(currentProjectMiddleware, userToInvite);
        inviteUserMiddleware.execute(null);
        return userToInvite;
    }

    public static void acceptInvite(int inviteIndex){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth acceptInviteMiddleware = new AcceptInviteDecorator(authMiddleware, inviteIndex);
        acceptInviteMiddleware.execute(null);
    }

    public static void rejectInvite(int inviteIndex){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth rejectInviteMiddleware = new RejectInviteDecorator(authMiddleware, inviteIndex);
        rejectInviteMiddleware.execute(null);
    }
}
