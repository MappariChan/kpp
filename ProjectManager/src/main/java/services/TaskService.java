package services;

import db.Database;
import entities.Task;
import services.authdecorator.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskService {
    public static Task addTask(String stageName, String title, Date dateStart, Date deadline, int priority){
        Task taskToAdd = new Task();
        taskToAdd.setTitle(title);
        taskToAdd.setDateStart(dateStart);
        taskToAdd.setDateEnd(null);
        taskToAdd.setDeadline(deadline);
        taskToAdd.setPriority(priority);
        taskToAdd.setDone(false);
        taskToAdd.setSubTasks(new ArrayList<>());
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth addTaskMiddleware = new AddTaskDecorator(currentProjectMiddleware, stageName, taskToAdd);
        addTaskMiddleware.execute(null);
        return taskToAdd;
    }

    public static List<Task> getTasks(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth getTasksMiddleware = new GetTasksDecorator(currentProjectMiddleware);
        getTasksMiddleware.execute(null);
        return  ((GetTasksDecorator)getTasksMiddleware).getTasks();
    }

    public static Task openTask(String taskTitle){
        return getTasks().stream().filter(task -> task.getTitle().equals(taskTitle)).findFirst().get();
    }

    public static void nextStage(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth currentTaskMiddleware = new AccessTaskDataDecorator(currentProjectMiddleware);
        IAuth nextStageMiddleware = new TaskToNextStageDecorator(currentTaskMiddleware);
        nextStageMiddleware.execute(null);
    }

    public static void makeTask(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth currentTaskMiddleware = new AccessTaskDataDecorator(currentProjectMiddleware);
        IAuth makeTaskMiddleware = new MakeTaskDecorator(currentTaskMiddleware);
        makeTaskMiddleware.execute(null);
    }
}
