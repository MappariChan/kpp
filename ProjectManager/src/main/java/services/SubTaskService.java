package services;

import db.Database;
import entities.SubTask;
import services.authdecorator.*;

public class SubTaskService {
    public static SubTask addSubTask(String subTaskTitle){
        SubTask subTask = new SubTask();
        subTask.setDone(false);
        subTask.setTitle(subTaskTitle);
        Database.getInstance().getDatastore().save(subTask);
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth currentTaskMiddleware = new AccessTaskDataDecorator(currentProjectMiddleware);
        IAuth addSubTaskMiddleware = new AddSubTaskDecorator(currentTaskMiddleware, subTask);
        addSubTaskMiddleware.execute(null);
        return subTask;
    }

    public static void makeSubTask(String subTaskTitle){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth currentProjectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth currentTaskMiddleware = new AccessTaskDataDecorator(currentProjectMiddleware);
        IAuth makeSubTaskMiddleware = new MakeSubTaskDecorator(currentTaskMiddleware, subTaskTitle);
        makeSubTaskMiddleware.execute(null);
    }
}
