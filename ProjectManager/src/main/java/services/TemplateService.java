package services;

import entities.Template;
import services.authdecorator.*;

import java.util.List;

public class TemplateService {
    public static Template saveTemplate(String templateName){
        Template template = new Template();
        template.setName(templateName);
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth projectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth saveTemplateMiddleware = new SaveTemplateDecorator(projectMiddleware, template);
        saveTemplateMiddleware.execute(null);
        return template;
    }

    public static List<Template> getTemplates(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth projectMiddleware = new AccessProjectDataDecorator(authMiddleware);
        IAuth getTemplatesMiddleware = new GetTemplatesDecorator(projectMiddleware);
        getTemplatesMiddleware.execute(null);
        return ((GetTemplatesDecorator)getTemplatesMiddleware).getTemplates();
    }

    public static Template getTemplate(String templateName){
        return getTemplates().stream().filter(template -> template.getName().equals(templateName)).findFirst().get();
    }
}
