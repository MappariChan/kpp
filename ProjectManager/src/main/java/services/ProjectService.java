package services;

import db.Database;
import entities.*;
import org.mongodb.morphia.query.Query;
import services.authdecorator.*;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {
    public static Project addProject(String name){
        Project project = new Project();
        project.setName(name);
        project.setStages(new ArrayList<Stage>());
        project.setTasks(new ArrayList<Task>());
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth addProjectMiddleware = new AddProjectDecorator(authMiddleware, project);
        addProjectMiddleware.execute(null);
        return project;
    }

    public static Project addProject(String name, Template template){
        Project project = new Project();
        project.setName(name);
        project.setStages(template.getStages());
        project.setTasks(new ArrayList<Task>());
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth addProjectMiddleware = new AddProjectDecorator(authMiddleware, project);
        addProjectMiddleware.execute(null);
        return project;
    }

    public static List<Project> getProjects(){
        IAuth authMiddleware = new AccessDataDecorator(new Auth());
        IAuth getProjectMiddleware = new GetProjectsDecorator(authMiddleware);
        getProjectMiddleware.execute(null);
        return ((GetProjectsDecorator)getProjectMiddleware).getProjects();
    }

    public static Project openProject(String name){
        return getProjects().stream().
                filter(project -> project.getName().equals(name)).
                findFirst().get();
    }
}
