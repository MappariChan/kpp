package services;

import entities.Invite;
import entities.Project;
import entities.Template;
import entities.User;
import db.Database;
import org.mongodb.morphia.query.*;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

public class UserService {
    public static User register(String login, String email, String password){
        User newUser = new User();
        newUser.setLogin(login);
        newUser.setEmail(email);
        newUser.setPassword(password);
        newUser.setProjectList(new ArrayList<Project>());
        newUser.setInvites(new ArrayList<Invite>());
        newUser.setTemplates(new ArrayList<Template>());
        Database.getInstance().getDatastore().save(newUser);
        return newUser;
    }
    public static User login(String login, String password){
        Query<User> query = Database.
                getInstance().
                getDatastore().
                createQuery(User.class);
        query.and(
                query.or(
                    query.criteria("login").equal(login),
                    query.criteria("email").equal(login)
                ),
                query.criteria("password").equal(password)
        );
       return query.get();
    }
}
