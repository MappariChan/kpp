package main;
import command.*;

import java.util.logging.Level;
import java.util.logging.Logger;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    //TODO add subtask
    //TODO add errors to all services
    //TODO add normal task printing
    public static void main(String[] args) {
        Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
        mongoLogger.setLevel(Level.OFF);
        Logger morphiaLogger = Logger.getLogger("org.mongodb.morphia");
        morphiaLogger.setLevel(Level.OFF);
        Command command = invokeCommand(args[0]);
        command.execute();
    }

    private static Command invokeCommand(String command){
        switch (command){
            case "help":
                return new HelpCommand();
            case "login":
                return new LoginCommand();
            case "register":
                return new RegisterCommand();
            case "add_project":
                return new AddProjectCommand();
            case "project_list":
                return new GetProjectCommand();
            case "open":
                return new OpenProjectCommand();
            case "add_task":
                return new AddTaskCommand();
            case "invite":
                return new InviteUserCommand();
            case "invite_list":
                return new GetInviteListCommand();
            case "accept":
                return new AcceptInviteCommand();
            case "reject":
                return new RejectInviteCommand();
            case "task_list":
                return new GetTaskListCommand();
            case "add_stage":
                return new AddStageCommand();
            case "stage_list":
                return new GetStagesCommand();
            case "next_stage":
                return new NextStageCommand();
            case "make":
                return new MakeTaskCommand();
            case "save_template":
                return new SaveTemplateCommand();
            case "open_task":
                return new OpenTaskCommand();
            case "add_sub_task":
                return new AddSubTaskCommand();
            case "make_sub_task":
                return new MakeSubTaskCommand();
        }
        return null;
    }
}