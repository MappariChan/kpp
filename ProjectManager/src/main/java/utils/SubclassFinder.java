package utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class SubclassFinder {

    public static<T> ArrayList<Class<? extends T>> getSubclasses(Class<T> superClass){
        ArrayList<Class<? extends T>> subclasses = new ArrayList<>();
        String packageName = superClass.getPackage().getName();
        ArrayList<String> classNames;
        try{
            classNames = getClassesInPackage(packageName);
        }catch (IOException | ClassNotFoundException e){
            System.out.println(e.getMessage());
            return  new ArrayList<>();
        }


        for (String className : classNames) {
            try {
                if(className.contains(packageName)) {
                    Class<?> cls = Class.forName(className);
                    if (superClass.isAssignableFrom(cls) && !superClass.equals(cls) && !Modifier.isAbstract(cls.getModifiers())) {
                        Class<? extends T> subClass = (Class<? extends T>) cls;
                        subclasses.add(subClass);
                    }
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return subclasses;
    }

    private static ArrayList<String> getClassesInPackage(String packageName) throws IOException, ClassNotFoundException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        ArrayList<File> dirs = new ArrayList<>();

        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }

        ArrayList<String> classNames = new ArrayList<>();

        for (File directory : dirs) {
            classNames.addAll(findClasses(directory.getName(), packageName));
        }

        return classNames;
    }

    private static ArrayList<String> findClasses(File directory, String packageName) throws ClassNotFoundException {
        ArrayList<String> classNames = new ArrayList<>();
        if (!directory.exists()) {
            return classNames;
        }

        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                classNames.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classNames.add(packageName + '.' + file.getName().substring(0, file.getName().length() - 6));
            }
        }

        return classNames;
    }

    private static ArrayList<String> findClasses(String path, String packageName) throws ClassNotFoundException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resources = null;
        try{
            resources = classLoader.getResources(path);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        ArrayList<String> classNames = new ArrayList<>();

        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();

            if (resource.getProtocol().equals("jar")) {
                // If the resource is within a JAR
                JarFile jarFile = null;
                try {
                    JarURLConnection jarConnection = (JarURLConnection) resource.openConnection();
                    jarFile = jarConnection.getJarFile();
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
                Enumeration<JarEntry> entries = jarFile.entries();

                while (entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();

                    if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
                        String className = entry.getName().replace('/', '.').substring(0, entry.getName().length() - 6);
                        classNames.add(className);
                    }
                }
            } else if (resource.getProtocol().equals("file")) {
                // If the resource is a file (not in a JAR)
                File file = new File(resource.getFile());
                classNames.addAll(findClasses(file, packageName));
            }
        }
        return classNames;
    }

}
