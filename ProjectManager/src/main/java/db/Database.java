package db;

import com.mongodb.MongoClient;
import org.mongodb.morphia.*;

public class Database {
    private static Database db;
    private Datastore datastore;

    public Datastore getDatastore() {
        return datastore;
    }

    private Database(){
        MongoClient client = new MongoClient("127.0.0.1", 27017);
        Morphia morphia = new Morphia();
        datastore = morphia.createDatastore(client, "ProjectManager");
    }

    public static Database getInstance(){
        if(db == null){
            db = new Database();
        }
        return db;
    }
}
