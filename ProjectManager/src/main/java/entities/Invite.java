package entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;
import services.ProjectService;

@Entity("invites")
public class Invite {
    @Id
    private ObjectId id;
    @Reference
    private User from;
    @Reference
    private Project to;

    public Project getTo() {
        return to;
    }

    public void setTo(Project to) {
        this.to = to;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "From: " + from + "; To: " + to +".";
    }
}
