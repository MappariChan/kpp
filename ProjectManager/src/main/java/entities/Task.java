package entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity("tasks")
public class Task
{
    @Id
    private ObjectId id;
    private String title;
    private Date dateStart;
    private Date dateEnd;
    private Date deadline;
    private int priority;
    private boolean done;
    @Reference
    private Stage currentStage;
    @Reference
    private List<SubTask> subTasks;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Stage getCurrentStage() {
        return currentStage;
    }

    public void setCurrentStage(Stage currentStage) {
        this.currentStage = currentStage;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public boolean isDone() {
        return done;
    }

    public List<SubTask> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTask> subTasks) {
        this.subTasks = subTasks;
    }

    @Override
    public String toString() {
        String priorityString = "";
        for (int i = 0; i < priority; i++) {
            priorityString = priorityString + "*";
        }
        for (int i = 0; i < 5 - priority; i++) {
            priorityString = priorityString + "_";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String taskString = String.format("%s %s %s - %s%s", title, priorityString, dateFormat.format(dateStart), dateFormat.format(deadline), done ? " Done at - " + dateFormat.format(dateEnd) : "");
        return taskString;
    }
}