package command;

import entities.SubTask;
import services.SubTaskService;

import java.util.Scanner;

public class AddSubTaskCommand extends Command{
    @Override
    public String getDescription() {
        return "add_sub_task - adds sub task to the current task.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter sub task title:");
        String subTaskTitle = scanner.nextLine();

        SubTask subTask = SubTaskService.addSubTask(subTaskTitle);

        System.out.println("'" + subTask.getTitle() + "' was added.");
    }
}
