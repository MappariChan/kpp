package command;

import entities.Task;
import services.TaskService;

import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;
import java.text.SimpleDateFormat;

public class AddTaskCommand extends Command{
    @Override
    public void execute() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter stage name:");
        String stageName = scanner.nextLine();
        System.out.println("Enter task title:");
        String taskTitle = scanner.nextLine();
        System.out.println("Enter task date of start (yyyy-mm-dd):");
        String dateStartString = scanner.nextLine();
        Date dateStart = null;
        try {
            dateStart = dateFormat.parse(dateStartString);
        }catch (ParseException e){
            e.printStackTrace();
        }
        System.out.println("Enter task deadline (yyyy-mm-dd):");
        String deadlineString = scanner.nextLine();
        Date deadline = null;
        try {
            deadline = dateFormat.parse(deadlineString);
        }catch (ParseException e){
            e.printStackTrace();
        }
        System.out.println("Enter priority (from 1 to 5):");
        int priority = scanner.nextInt();


        Task addedTask = TaskService.addTask(stageName, taskTitle, dateStart, deadline, priority);

        System.out.println("'" + addedTask.getTitle() + "' task was successfully added to the project.");
    }

    @Override
    public String getDescription() {
        return "add_task - adds task to the opened project.";
    }
}
