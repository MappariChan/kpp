package command;

import entities.Template;
import services.TemplateService;

import java.util.Scanner;

public class SaveTemplateCommand extends Command{
    @Override
    public String getDescription() {
        return "save_template = saves template of project stages for current user.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter template name:");
        String templateName = scanner.nextLine();

        Template template = TemplateService.saveTemplate(templateName);
        System.out.println("'" + template.getName() + "' was successfully saved.");
    }
}
