package command;

import services.TaskService;

import java.util.Scanner;

public class MakeTaskCommand extends Command{
    @Override
    public String getDescription() {
        return "make - makes task.";
    }

    @Override
    public void execute() {
        TaskService.makeTask();
    }
}
