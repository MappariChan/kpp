package command;

import entities.Stage;
import entities.SubTask;
import entities.Task;
import org.bson.types.ObjectId;
import services.StageService;
import services.TaskService;

import java.io.Console;
import java.util.List;
import java.util.stream.Collectors;

public class GetTaskListCommand extends Command{
    @Override
    public String getDescription() {
        return "task_list - returns list of tasks in the current project";
    }

    @Override
    public void execute() {
        List<Stage> stageList = StageService.getStages();
        List<Task> taskList = TaskService.getTasks();
        int stageNumber = 1;
        for(Stage stage : stageList) {
            List<Task> currentSatgeTaskList = taskList.stream().filter(task -> task.getCurrentStage().getName().equals(stage.getName())).collect(Collectors.toList());
            System.out.println("\t" + stageNumber + ". " + stage + ":");
            int taskNumber = 1;
            for (Task task : currentSatgeTaskList) {
                System.out.println("\t\t" + taskNumber + ". " + task + ";");
                int subTaskNumber = 1;
                for(SubTask subTask : task.getSubTasks()){
                    System.out.println("\t\t\t" + subTaskNumber + ". " + subTask + ";");
                    subTaskNumber++;
                }
                taskNumber++;
            }
            stageNumber++;
        }
    }
}
