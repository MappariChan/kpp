package command;

import services.SubTaskService;

import java.util.Scanner;

public class MakeSubTaskCommand extends Command{
    @Override
    public String getDescription() {
        return "make_sub_task - makes sub task of the opened task.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter sub task title to be done:");
        String title = scanner.nextLine();

        SubTaskService.makeSubTask(title);
    }
}
