package command;

import entities.User;
import services.InviteService;
import services.ProjectService;

import java.util.Scanner;

public class InviteUserCommand extends Command{
    @Override
    public String getDescription() {
        return "invite - invites user to the current project.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter user to invite login or email:");
        String userName = scanner.nextLine();

        User userToInvite = InviteService.inviteUser(userName);

        System.out.println("'" + userToInvite.getLogin() + "' was successfully invited.");
    }
}
