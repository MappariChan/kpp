package command;

import entities.Project;
import services.ProjectService;

import java.util.List;

public class GetProjectCommand extends Command{
    @Override
    public void execute() {
        List<Project> projects =  ProjectService.getProjects();
        System.out.println("Your projects:");
        int orderNumber = 1;
        for(Project project : projects){
            System.out.println("\t" + orderNumber + ". " + project);
            orderNumber++;
        }
    }

    @Override
    public String getDescription() {
        return "project_list - shows list of current user projects.";
    }
}
