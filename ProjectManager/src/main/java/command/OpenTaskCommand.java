package command;

import entities.Task;
import services.TaskService;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class OpenTaskCommand extends Command{
    @Override
    public String getDescription() {
        return "open_task - opens task.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter task title:");
        String taskTitle = scanner.nextLine();

        Task taskToOpen = TaskService.openTask(taskTitle);

        String fileName = "C:\\localstorage\\task.txt";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(taskToOpen.getTitle());
            System.out.println(taskToOpen.getTitle() + " was opened.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
