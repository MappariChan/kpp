package command;

import entities.Stage;
import entities.Template;
import services.ProjectService;
import services.TemplateService;

import java.util.List;
import java.util.Scanner;

public class AddProjectCommand extends Command{
    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter project name:");
        String name = scanner.nextLine();

        System.out.println("Do you want to use template for your project? (yes/no)");
        String answer = scanner.nextLine();
        if(answer.equals("yes")){
            System.out.println("Your templates:");
            List<Template> templates = TemplateService.getTemplates();
            int templateOrderNumber = 1;
            for (Template template : templates) {
                System.out.println("\t" + templateOrderNumber + ". " + template + ";");
                templateOrderNumber++;
            }
            System.out.println("Enter template name:");
            String templateName = scanner.nextLine();
            Template template = TemplateService.getTemplate(templateName);
            ProjectService.addProject(name, template);
            return;
        }

        ProjectService.addProject(name);
    }

    @Override
    public String getDescription() {
        return "add_project - creates your new project.";
    }
}
