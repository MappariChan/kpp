package command;

import entities.Stage;
import services.StageService;

import java.util.Scanner;

public class AddStageCommand extends Command {
    @Override
    public String getDescription() {
        return "add_stage - adds task stage to the current project.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter stage name:");
        String stageName = scanner.nextLine();
        System.out.println("Enter stage order number:");
        int stageOrderNumber = scanner.nextInt();

        Stage stage = StageService.addStage(stageName, stageOrderNumber - 1);
        System.out.println("'"+ stage.getName() + "' was successfully added to the project.");
    }
}
