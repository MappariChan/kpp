package command;

import entities.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import main.Main;
import services.UserService;

import java.io.*;
import java.util.Scanner;

public class LoginCommand extends Command {

    public LoginCommand(){}
    @Override
    public String getDescription() {
        return "login - logins user into the system.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter login or email:");
        String login = scanner.nextLine();
        System.out.println("Enter password:");
        String password = scanner.nextLine();

        User user = UserService.login(login, password);

        String fileName = "C:\\localstorage\\token.txt"; // Specify the file name

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            String token = Jwts.builder()
                        .setSubject(user.getEmail())
                        .signWith(SignatureAlgorithm.HS256, "my-secret-key")
                        .compact();

                writer.write(token);
                System.out.println("Hello, " + user.getLogin() + ".");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
