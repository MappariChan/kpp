package command;

import java.lang.reflect.Constructor;

public abstract class Command {
    public abstract void execute();
    public abstract String getDescription();
    public static Command getInstance(Class<? extends Command> subclass){
        Constructor<? extends Command> constructor;
        try {
            constructor = subclass.getDeclaredConstructor();
        }
        catch (NoSuchMethodException e){
            System.out.println(e.getMessage());
            return null;
        }
        constructor.setAccessible(true);
        Command instance = null;
        try{
            instance = constructor.newInstance();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return instance;
    }
}
