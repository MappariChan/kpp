package command;

import entities.Invite;
import services.InviteService;

import java.util.List;

public class GetInviteListCommand extends Command{
    @Override
    public String getDescription() {
        return "invite_list - returns list of your invites.";
    }

    @Override
    public void execute() {
        List<Invite> invites = InviteService.getInvites();
        System.out.println("Your invites:");
        int number = 1;
        for(Invite invite : invites){
            System.out.println("\t" + number +". " + invite);
            number++;
        }
    }
}
