package command;

import entities.Project;
import entities.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import services.ProjectService;
import services.UserService;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class OpenProjectCommand extends Command {
    @Override
    public String getDescription() {
        return "open - opens project by name.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter project name:");
        String projectName = scanner.nextLine();

        Project project = ProjectService.openProject(projectName);

        String fileName = "C:\\localstorage\\project.txt";

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(project.getName());
            System.out.println(project.getName() + " was opened.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
