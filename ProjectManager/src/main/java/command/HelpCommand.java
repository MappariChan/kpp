package command;

import utils.SubclassFinder;

import java.util.ArrayList;

public class HelpCommand extends Command{
    private final ArrayList<Class<? extends Command>> commands;
    public HelpCommand(){
        super();
        commands = SubclassFinder.getSubclasses(Command.class);
    }
    @Override
    public void execute(){
        for(Class<? extends Command> command : commands){
            System.out.println(Command.getInstance(command).getDescription());
        }
    }
    @Override
    public String getDescription(){
        return "help - this command returns list of all commands.";
    }
}
