package command;

import entities.Stage;
import services.StageService;

import java.io.Console;
import java.util.List;

public class GetStagesCommand extends Command{
    @Override
    public String getDescription() {
        return "stage_list - returns list of stages in this project.";
    }

    @Override
    public void execute() {
        List<Stage> stages = StageService.getStages();
        System.out.println("Your project stages:");
        int stageOrderNumber = 1;
        for(Stage stage : stages){
            System.out.println("\t" + stageOrderNumber + ". " + stage);
        }
    }
}
