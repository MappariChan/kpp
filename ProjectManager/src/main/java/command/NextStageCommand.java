package command;

import services.TaskService;

import java.util.Scanner;

public class NextStageCommand extends Command{
    @Override
    public String getDescription() {
        return "next_stage - moves task to the next stage";
    }

    @Override
    public void execute() {
        TaskService.nextStage();
    }
}
