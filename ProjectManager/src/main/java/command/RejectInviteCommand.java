package command;

import services.InviteService;

import java.util.Scanner;

public class RejectInviteCommand extends Command{
    @Override
    public String getDescription() {
        return "reject - rejects invite to the project.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter invite number:");
        int inviteIndex = scanner.nextInt() - 1;

        InviteService.rejectInvite(inviteIndex);

        System.out.println("Invite rejected.");
    }
}
