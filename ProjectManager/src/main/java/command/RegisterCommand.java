package command;

import java.util.Scanner;

import entities.User;
import services.UserService;

public class RegisterCommand extends Command {

    public RegisterCommand(){}
    @Override
    public String getDescription() {
        return "register - registers user in the system.";
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter login:");
        String login = scanner.nextLine();
        System.out.println("Enter email:");
        String email = scanner.nextLine();
        System.out.println("Enter password:");
        String password = scanner.nextLine();

        User newUser = UserService.register(login, email, password);
        System.out.println("'" + newUser.getLogin() + "' was successfully registered.");
    }
}
