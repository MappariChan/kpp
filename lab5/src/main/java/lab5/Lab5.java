package lab5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lab5 {
    public static void Task1(String rawString, char firstLetter){
        String[] sentences = rawString.split("[.?!]\s*");
        Pattern sentencePattern = Pattern.compile("\s+");
        Pattern wordPattern = Pattern.compile("^" + firstLetter);
        for(String sentence : sentences){
            Matcher sentenceMatcher = sentencePattern.matcher(sentence);
            if(sentenceMatcher.find()){
                String[] words = sentence.split("[,:;-]?\s+");
                String secondWord = words[1];
                Matcher wordMatcher = wordPattern.matcher(secondWord);
                if(wordMatcher.find()){
                    System.out.println(secondWord);
                }
            }
        }
    }

    public static void Task2(String rawString, int wordLength){
        String[] sentences = rawString.split("(?<=[.?!])\s*");
        Pattern sentencePattern = Pattern.compile("\\?$");
        Pattern wordPattern = Pattern.compile(String.format("^[a-zA-Z]{%d}$", wordLength));
        Pattern cutAskSignPattern = Pattern.compile("^(.*)\\?$");
        for(String sentence : sentences){
            Matcher sentenceMatcher = sentencePattern.matcher(sentence);
            if(sentenceMatcher.find()){
                Matcher cutAskSignMatcher = cutAskSignPattern.matcher(sentence);
                if(cutAskSignMatcher.matches()) {
                    String[] words = cutAskSignMatcher.group(1).split("[,:;?-]?\s+");
                    for(String word : words) {
                        Matcher wordMatcher = wordPattern.matcher(word);
                        if (wordMatcher.find()) {
                            System.out.println(word);
                        }
                    }
                }
            }
        }
    }

    public static void Task3(String rawString){
        String[] sentences = rawString.split("(?<=[.?!])\s*");
        Pattern sentencePattern = Pattern.compile("^(.*\s+of)\s([^.]+\s*.*[.?!])");
        for(String sentence : sentences){
            Matcher sentenceMatcher = sentencePattern.matcher(sentence);
            while(sentenceMatcher.find()){
                sentence = sentenceMatcher.group(1) + sentenceMatcher.group(2);
                sentenceMatcher = sentencePattern.matcher(sentence);
            }
            System.out.println(sentence);
        }
    }
}
