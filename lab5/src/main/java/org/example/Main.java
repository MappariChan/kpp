package org.example;

import lab5.Lab5;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        String rawString = "In a bustling city, the streets echoed with the sounds of life." +
                " People hurried along, lost in their thoughts, while the sun painted the skyline with warm hues of ?" +
                " At a nearby cafe, friends of gathered to of share laughter and stories over steaming cups of coffee." +
                " Meanwhile, in a quiet park, a musician played a soothing melody on his guitar, creating a peaceful oasis amidst the urban chaos." +
                " A gentle breeze rustled the leaves, carrying with it the scent of blooming flower?" +
                " In an old bookstore, shelves lined with books of held the promise of countless adventures and untold knowledge." +
                " As night fell, the cityscape transformed into a glittering tapestry, with lights illuminating the buildings like a sea of stars." +
                " Far away, the ocean waves whispered their timeless secrets to the shore, a reminder of nature's eternal dance." +
                " Underneath the vast expanse of the night sky, dreams took flight, of carried by the promise of a new day of ." +
                " Answer, anyway?";
        System.out.println("Task1:");
        Lab5.Task1(rawString, 'a');
        System.out.println("Task2:");
        Lab5.Task2(rawString, 6);
        System.out.println("Task3:");
        Lab5.Task3(rawString);
    }
}