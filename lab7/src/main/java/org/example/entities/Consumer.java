package org.example.entities;

import java.util.ArrayList;

public class Consumer {
    private ArrayList<Good> goods;

    public Consumer(){
        goods = new ArrayList<>();
    }

    public void buyGood(Factory factory){
        goods.add(factory.produceGoods());
    }
}
