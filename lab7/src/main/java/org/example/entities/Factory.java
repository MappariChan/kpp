package org.example.entities;

import java.util.Stack;

public class Factory {
    private Stack<Resource> resources;

    public Factory(){
        resources = new Stack<>();
    }

    public Resource useResources(){
        return resources.pop();
    }

    public Good produceGoods(){
        return new Good(useResources());
    }
}
