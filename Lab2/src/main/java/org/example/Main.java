package org.example;
import airline.*;

import java.util.*;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParsePosition;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        ParsePosition pos = new ParsePosition(0);
        ArrayList<Plane> planes = new ArrayList<>();

        planes.add(new Plane("Boeing 737", "737-800", 5000.0, sdf.parse("01-01-2022", pos), 35.5, 100));
        pos.setIndex(0);
        planes.add(new Plane("Embraer E190", "E190-E2", 4500.0, sdf.parse("03-01-2022", pos), 33.0, 100));
        pos.setIndex(0);
        planes.add(new Plane("Airbus A320", "A320neo", 6000.0, sdf.parse("02-01-2022", pos), 37.0, 100));
        pos.setIndex(0);
        planes.add(new Plane("Bombardier CRJ700", "CRJ700", 3000.0, sdf.parse("04-01-2022", pos), 32.0, 100));
        pos.setIndex(0);
        planes.add(new Plane("Boeing 787", "787-9", 9000.0, sdf.parse("05-01-2022", pos), 62.0, 100));
        pos.setIndex(0);
        planes.add(new Plane("Airbus A350", "A350-900", 8000.0, sdf.parse("06-01-2022", pos), 64.5, 100));
        pos.setIndex(0);
        planes.add(new Plane("Embraer E195", "E195-E2", 4700.0, sdf.parse("07-01-2022", pos), 36.8, 100));
        pos.setIndex(0);
        planes.add(new Plane("Bombardier CRJ900", "CRJ900", 3300.0, sdf.parse("08-01-2022", pos), 36.9, 100));
        pos.setIndex(0);
        planes.add(new Plane("Boeing 777", "777-200LR", 11000.0, sdf.parse("09-01-2022", pos), 63.7, 100));
        pos.setIndex(0);
        planes.add(new Plane("Airbus A380", "A380-800", 14000.0, sdf.parse("10-01-2022", pos), 72.7, 100));

        ArrayList<Flight> flights = new ArrayList<>();
        AirlineManager airlineManager = new AirlineManager("MatviiAirlines", flights, planes);

        System.out.println("Total carrying capacity:");
        System.out.println(airlineManager.getTotalCarryingCapacity());
        System.out.println();

        System.out.println("Sort planes by max flight distance ascending:");
        ArrayList<Plane> sortedPlanes = airlineManager.sortPlanesByMaxFlightDistance(Order.Ascending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by max flight distance descending:");
        sortedPlanes = airlineManager.sortPlanesByMaxFlightDistance(Order.Descending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by date of creation ascending:");
        sortedPlanes = airlineManager.sortPlanesByDateOfCreation(Order.Ascending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by date of creation descending:");
        sortedPlanes = airlineManager.sortPlanesByDateOfCreation(Order.Descending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by length ascending:");
        sortedPlanes = airlineManager.sortPlanesByLength(Order.Ascending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by length descending:");
        sortedPlanes = airlineManager.sortPlanesByLength(Order.Descending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by name ascending:");
        sortedPlanes = airlineManager.sortPlanesByName(Order.Ascending);
        PrintArrayList(sortedPlanes);
        System.out.println("Sort planes by name descending:");
        sortedPlanes = airlineManager.sortPlanesByName(Order.Descending);
        PrintArrayList(sortedPlanes);
    }

    private static <T> void PrintArrayList(ArrayList<T> arrayListToPrint){
//        for(T element : arrayListToPrint){
//            System.out.println(element);
//        }
//        System.out.println();
        ListIterator<T> myListIterator = arrayListToPrint.listIterator();
        while(myListIterator.hasNext()){
            myListIterator.next();
        }
        while(myListIterator.hasPrevious()){
            System.out.println(myListIterator.previous());
        }
        System.out.println();
    }
}