package airline;

public class Flight {
    private Route route;
    private Plane plane;

    public Flight(Route route, Plane plane){
        this.route = route;
        this.plane = plane;
    }

    public Route getRoute(){
        return route;
    }
    public void setRoute(Route route){
        this.route = route;
    }
    public Plane getPlane(){
        return plane;
    }
    public void setPlane(Plane plane){
        this.plane = plane;
    }
}
