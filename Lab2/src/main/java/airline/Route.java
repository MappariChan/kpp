package airline;

public class Route {
    private Airport startPoint;
    private Airport endPoint;

    public Route(Airport startPoint, Airport endPoint){
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public Airport getStartPoint(){
        return startPoint;
    }
    public void setStartPoint(Airport startPoint){
        this.startPoint = startPoint;
    }
    public Airport getEndPoint(){
        return endPoint;
    }
    public void setEndPoint(Airport endPoint){
        this.endPoint = endPoint;
    }
}
