package airline;

import java.util.ArrayList;
import java.util.Date;

public class CargoPlane extends Plane{
    private ArrayList<String> cargo;

    public CargoPlane(String name, String model, double maxFlightDistance, Date dateOfCreation, double length, double carryingCapacity, ArrayList<String> cargo){
        super(name, model, maxFlightDistance, dateOfCreation, length, carryingCapacity);
        this.cargo = cargo;
    }

    public ArrayList<String> getCargo(){
        return cargo;
    }
    public void setCargo(ArrayList<String> cargo){
        this.cargo = cargo;
    }
}
