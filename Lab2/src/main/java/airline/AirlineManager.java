package airline;

import java.util.*;

public class AirlineManager {
    private String name;
    private ArrayList<Flight> flights;
    private ArrayList<Plane> planes;

    public AirlineManager(String name, ArrayList<Flight> flights, ArrayList<Plane> planes){
        this.name = name;
        this.flights = flights;
        this.planes = planes;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<Flight> getFlights() {
        return flights;
    }
    public void setFlights(ArrayList<Flight> flights) {
        this.flights = flights;
    }
    public ArrayList<Plane> getPlanes() {
        return planes;
    }
    public void setPlanes(ArrayList<Plane> planes) {
        this.planes = planes;
    }

    public double getTotalCarryingCapacity(){
        double totalCarryingCapacity = 0;
        for(Plane plane : planes){
            totalCarryingCapacity += plane.getCarryingCapacity();
        }
        return totalCarryingCapacity;
    }

    public ArrayList<Plane> sortPlanesByMaxFlightDistance(Order order){
        ArrayList<Plane> sortedPlanes = new ArrayList<>(planes);
        if(order == Order.Ascending) {
            Collections.sort(sortedPlanes, new AirlineManager.MaxFlightDistanceComparator());
        }
        else if(order == Order.Descending){
            Collections.sort(sortedPlanes, Collections.reverseOrder(new AirlineManager.MaxFlightDistanceComparator()));
        }
        return sortedPlanes;
    }

    public ArrayList<Plane> sortPlanesByDateOfCreation(Order order){
        ArrayList<Plane> sortedPlanes = new ArrayList<>(planes);
        if(order == Order.Ascending) {
            Collections.sort(sortedPlanes, new DateOfCreationComparator());
        }
        else if(order == Order.Descending){
            Collections.sort(sortedPlanes, Collections.reverseOrder(new DateOfCreationComparator()));
        }
        return sortedPlanes;
    }

    public ArrayList<Plane> sortPlanesByLength(Order order){
        ArrayList<Plane> sortedPlanes = new ArrayList<>(planes);
        Comparator<Plane> anonymousComparator = new Comparator<Plane>() {
            @Override
            public int compare(Plane plane1, Plane plane2) {
                return Double.compare(plane1.getLength(), plane2.getLength());
            }
        };
        if(order == Order.Ascending) {
            Collections.sort(sortedPlanes, anonymousComparator);
        }
        else if(order == Order.Descending){
            Collections.sort(sortedPlanes, Collections.reverseOrder(anonymousComparator));
        }
        return sortedPlanes;
    }

    public ArrayList<Plane> sortPlanesByName(Order order){
        ArrayList<Plane> sortedPlanes = new ArrayList<>(planes);
        if(order == Order.Ascending) {
            Collections.sort(sortedPlanes, (Plane plane1, Plane plane2) -> plane1.getName().compareTo(plane2.getName()));
        }
        else if(order == Order.Descending){
            Collections.sort(sortedPlanes, Collections.reverseOrder((Plane plane1, Plane plane2) -> plane1.getName().compareTo(plane2.getName())));
        }
        return sortedPlanes;
    }

    static class MaxFlightDistanceComparator implements Comparator<Plane> {
        public int compare(Plane plane1, Plane plane2){
            return Double.compare( plane1.getMaxFlightDistance(), plane2.getMaxFlightDistance());
        }
    }

    class DateOfCreationComparator implements Comparator<Plane>{
        public int compare(Plane plane1, Plane plane2){
            return plane1.getDateOfCreation().compareTo(plane2.getDateOfCreation());
        }
    }
}
