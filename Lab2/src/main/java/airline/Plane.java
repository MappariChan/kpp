package airline;

import java.util.Date;

public class Plane {
    private String name;
    private String model;
    private double maxFlightDistance;
    private Date dateOfCreation;
    private double length;
    private double carryingCapacity;

    public Plane(String name, String model, double maxFlightDistance, Date dateOfCreation, double length, double carryingCapacity){
        this.name = name;
        this.model = model;
        this.maxFlightDistance = maxFlightDistance;
        this.dateOfCreation = dateOfCreation;
        this.length = length;
        this.carryingCapacity = carryingCapacity;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getModel(){
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public double getMaxFlightDistance(){
        return maxFlightDistance;
    }
    public void setMaxFlightDistance(double maxFlightDistance) {
        this.maxFlightDistance = maxFlightDistance;
    }
    public Date getDateOfCreation(){
        return dateOfCreation;
    }
    public void setDateOfCreation(Date dateOfCreation){
        this.dateOfCreation = dateOfCreation;
    }
    public double getLength(){
        return length;
    }
    public void setLength(double length){
        this.length = length;
    }
    public double getCarryingCapacity(){
        return carryingCapacity;
    }
    public void setCarryingCapacity(double carryingCapacity){
        this.carryingCapacity = carryingCapacity;
    }

    @Override
    public String toString(){
        return name + "; " + model + "; " + maxFlightDistance + "; " + dateOfCreation.toString() + "; " + length;
    }
}
