package airline;

import java.util.Date;

public class PassengerPlane extends Plane {
    private int passengerSeatAmount;

    public PassengerPlane(String name, String model, double maxFlightDistance, Date dateOfCreation, double length, double carryingCapacity, int passengerSeatAmount){
        super(name, model, maxFlightDistance, dateOfCreation, length, carryingCapacity);
        this.passengerSeatAmount = passengerSeatAmount;
    }

    public int getPassengerSeatAmount(){
        return  passengerSeatAmount;
    }
    public void setPassengerSeatAmount(int passengerSeatAmount){
        this.passengerSeatAmount = passengerSeatAmount;
    }
}
