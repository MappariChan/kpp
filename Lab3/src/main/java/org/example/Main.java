package org.example;

import components.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        while(true) {
            try {
                while (Menu.getMenuInstance().Command());
                return;
            } catch (RuntimeException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }
}