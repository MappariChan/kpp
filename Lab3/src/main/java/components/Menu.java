package components;

import java.io.IOException;
import java.lang.reflect.AnnotatedArrayType;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;

public class Menu {
    private static Menu menuInstance;
    private ArrayList<Appliance> appliances;
    private final String path1 = "C:\\Users\\vantu\\Projects\\kpp\\Lab3\\text1.txt";
    private final String path2 = "C:\\Users\\vantu\\Projects\\kpp\\Lab3\\text2.txt";

    private Menu(){
        appliances = new ArrayList<>();
    }

    public static Menu getMenuInstance(){
        if(menuInstance == null){
            menuInstance = new Menu();
        }
        return menuInstance;
    }

    public boolean Command(){
        String command;
        Scanner scanner = new Scanner(System.in);
        command = scanner.nextLine();
        String[] words = command.split("\\s+");
        String internalCommand = words[0];
        if(internalCommand.equals("lab3")){
            String option = words[1];
            switch(option){
                case "add-appliance":
                    addAppliance(scanner);
                    return true;
                case "appliance-list":
                    printAppliances(appliances);
                    return true;
                case "create-producers-map":
                    createProducersMap(scanner);
                    return true;
                case "name-frequency-characteristic":
                    nameFrequencyCharacteristic();
                    return true;
                case "get-total-price":
                    getTotalPrice();
                    return true;
                case "exit":
                    return false;
                default:
                    throw new RuntimeException(String.format("'%s' is not recognized as an option of internal command\nuse 'help' option to see the list of available options", option));
            }
        }
        throw new RuntimeException(String.format("'%s' is not recognized as an internal command", internalCommand));
    }

    private void addAppliance(Scanner scanner){
        String applianceString = scanner.nextLine();
        Appliance newAppliance = new Appliance(applianceString);
        appliances.add(newAppliance);
    }

    private void printAppliances(ArrayList<Appliance> appliances){
        for(Appliance appliance : appliances){
            System.out.println(appliance);
        }
    }

    private void printAppliances(ArrayList<Appliance> appliances, int n){
        for(int i = 0; i < n; i++){
            if(i >= appliances.size()){
                return;
            }
            System.out.println(appliances.get(i));
        }
    }

    private void createProducersMap(Scanner scanner){
        int amount = scanner.nextInt();
        HashMap<String, ArrayList<Appliance>> producersMap = new HashMap<>();
        for(Appliance appliance : appliances){
            String producer = appliance.getProducer();
            if(!producersMap.containsKey(producer)) {
                producersMap.put(producer, new ArrayList<Appliance>());
                for (Appliance appliance1 : appliances) {
                    if(appliance1.getProducer().equals(producer)) {
                        producersMap.get(producer).add(appliance1);
                    }
                }
            }
        }
        for(String producer : producersMap.keySet()){
            System.out.println(producer);
            System.out.println();
            printAppliances(producersMap.get(producer), amount);
            System.out.println();
        }
    }

    private void nameFrequencyCharacteristic(){
        HashMap<String, Integer> nameFrequencyDict = new HashMap<>();
        for(Appliance appliance : appliances){
            String name = appliance.getName();
            if(!nameFrequencyDict.containsKey(name)){
                nameFrequencyDict.put(name, 1);
            }
            else{
                nameFrequencyDict.put(name, nameFrequencyDict.get(name) + 1);
            }
        }
        for(String name : nameFrequencyDict.keySet()){
            System.out.println(name + " - " + nameFrequencyDict.get(name).toString());
        }
    }

    private List<Appliance> readApplianceFromFiles(){
        ArrayList<Appliance> listReadFromFile = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(path1))) {
            String line;
            while ((line = br.readLine()) != null) {
                listReadFromFile.add(new Appliance(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(path2))) {
            String line;
            while ((line = br.readLine()) != null) {
                listReadFromFile.add(new Appliance(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Collections.sort(listReadFromFile, Collections.reverseOrder((Appliance appliance1, Appliance appliance2) -> appliance1.getPrice().compareTo(appliance2.getPrice())));
        List<Appliance> immutableList = Collections.unmodifiableList(listReadFromFile);
        return immutableList;
    }

    private void getTotalPrice(){
        List<Appliance> appliancesFromFile = readApplianceFromFiles();
        double totalPrice = 0;
        for(Appliance appliance : appliancesFromFile){
            totalPrice += appliance.getPrice();
        }
        System.out.println("Total price: " + totalPrice);
    }
}
