package components;

public class Appliance {
    private String name;
    private String model;
    private String producer;
    private Double price;
    private double maxPower;

    public Appliance(String name, String model, String producer, double price, double maxPower){
        this.name = name;
        this.model = model;
        this.producer = producer;
        this.price = price;
        this.maxPower = maxPower;
    }

    public Appliance(String applianceString){
        String[] applianceProperties = applianceString.split("\\s+");
        this.name = applianceProperties[0];
        this.model = applianceProperties[1];
        this.producer = applianceProperties[2];
        this.price = Double.parseDouble(applianceProperties[3]);
        this.maxPower = Double.parseDouble(applianceProperties[4]);
    }

    @Override
    public String toString(){
        return String.format("%-20s%-20s%-20s%-10.2f%-10.2f", name, model, producer, price, maxPower);
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getModel(){
        return model;
    }
    public void setModel(String model){
        this.model = model;
    }
    public String getProducer(){
        return producer;
    }
    public void setProducer(String producer){
        this.producer = producer;
    }
    public Double getPrice(){
        return price;
    }
    public void setPrice(double price){
        this.price = price;
    }
    public double getMaxPower(){
        return maxPower;
    }
    public void setMaxPower(double maxPower){
        this.maxPower = maxPower;
    }
}
