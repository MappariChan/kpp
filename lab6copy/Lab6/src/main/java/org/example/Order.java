package org.example;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class Order implements Serializable {
    @Serial
    private static final long serialVersionUID = 1037889123269134433L;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private List<Pizza> pizzas;
    private int orderNumber;
    private LocalDateTime orderDate;

    public Order(List<Pizza> pizzas, int orderNumber, LocalDateTime orderDate) {
        this.pizzas = new ArrayList<>(pizzas);
        this.orderDate = orderDate;
        this.orderNumber = orderNumber;
    }

    @Override
    public String toString() {
        String res = "\nPizzas: ";
        res += (pizzas.stream()
                .map(Pizza::getName)
                .collect(Collectors.joining(", ")));
        res += String.format("\nOrder number: %d, Delivery time: %s\n", orderNumber, orderDate.format(formatter));
        return res;
    }

    public void addPizza(Pizza pizza) {
        pizzas.add(pizza);
    }
}
