package org.example;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//        List<Pizza> pizzas = new ArrayList<>();
//        pizzas.add(new Pizza("Margherita", 200, 10, Arrays.asList("Tomato Sauce", "Mozzarella")));
//        pizzas.add(new Pizza("Pepperoni", 250, 12, Arrays.asList("Tomato Sauce", "Pepperoni", "Mozzarella")));
//        pizzas.add(new Pizza("Vegetarian", 220, 11, Arrays.asList("Tomato Sauce", "Mushrooms", "Bell Peppers", "Onions", "Mozzarella")));
//        pizzas.add(new Pizza("Hawaiian", 230, 12, Arrays.asList("Tomato Sauce", "Ham", "Pineapple", "Mozzarella")));
//        pizzas.add(new Pizza("BBQ Chicken", 260, 13, Arrays.asList("BBQ Sauce", "Chicken", "Red Onions", "Mozzarella")));
//        pizzas.add(new Pizza("Meat Lovers", 280, 14, Arrays.asList("Tomato Sauce", "Pepperoni", "Sausage", "Bacon", "Mozzarella")));
//        pizzas.add(new Pizza("Cheese Lover's", 240, 12, Arrays.asList("Alfredo Sauce", "Cheddar", "Mozzarella", "Parmesan")));
//        pizzas.add(new Pizza("Mushroom Delight", 210, 11, Arrays.asList("Creamy Garlic Sauce", "Mushrooms", "Spinach", "Mozzarella")));
//        pizzas.add(new Pizza("Buffalo Chicken", 250, 13, Arrays.asList("Buffalo Sauce", "Chicken", "Red Onions", "Blue Cheese", "Mozzarella")));
//        pizzas.add(new Pizza("Supreme", 270, 14, Arrays.asList("Tomato Sauce", "Pepperoni", "Sausage", "Mushrooms", "Bell Peppers", "Onions", "Black Olives", "Mozzarella")));
//        List<Order> orders1 = Arrays.asList(
//                new Order(Arrays.asList(pizzas.get(0), pizzas.get(1), pizzas.get(2), pizzas.get(5)), 1, LocalDateTime.now()),
//                new Order(Arrays.asList(pizzas.get(0), pizzas.get(2), pizzas.get(5)), 2, LocalDateTime.now().minusDays(1)),
//                new Order(Arrays.asList(pizzas.get(0), pizzas.get(1)), 3, LocalDateTime.now().minusDays(2)),
//                new Order(Arrays.asList(pizzas.get(0), pizzas.get(0), pizzas.get(2), pizzas.get(5)), 4, LocalDateTime.now().minusDays(3)),
//                new Order(Arrays.asList(pizzas.get(1), pizzas.get(5)), 5, LocalDateTime.now().minusDays(4))
//        );
//        List<Order> orders2 = Arrays.asList(new Order(Arrays.asList(pizzas.get(0), pizzas.get(1), pizzas.get(2)), 6, LocalDateTime.now().minusDays(5)),
//                new Order(Arrays.asList(pizzas.get(0)), 7, LocalDateTime.now().minusDays(6)),
//                new Order(Arrays.asList(pizzas.get(1), pizzas.get(1), pizzas.get(1), pizzas.get(5)), 8, LocalDateTime.now().minusDays(7)),
//                new Order(Arrays.asList(pizzas.get(2), pizzas.get(2)), 9, LocalDateTime.now().minusDays(8)),
//                new Order(Arrays.asList(pizzas.get(0), pizzas.get(1)), 10, LocalDateTime.now().minusDays(9)));
//
//        List<Visitor> visitors = Arrays.asList(
//                new Visitor("Vyhovs`koho 12", orders1),
//                new Visitor("Symonenka 11", orders2),
//                new Visitor("Bandery 10", new ArrayList<>())
//        );
//
//        Pizzeria pizzeria = new Pizzeria(pizzas, visitors);
//        pizzeria.serialize("pizzeria.ser");
        Pizzeria pizzeria = Pizzeria.deserialize("pizzeria.ser");
        System.out.println(pizzeria);

        while (true) {
            System.out.println("\nPizzeria Manager Menu:");
            System.out.println("1. Task 1");
            System.out.println("2. Task 2");
            System.out.println("3. Task 3");
            System.out.println("4. Task 4");
            System.out.println("5. Task 5");
            System.out.println("6. Task 6");
            System.out.println("7. Make order done");
            System.out.println("8. Exit");

            int choice = getUserChoice();

            switch (choice) {
                case 1:
                    System.out.println("Sorted orders by delivery time:");
                    StringBuilder builder = new StringBuilder();
                    for (Order value : pizzeria.sortOrdersByDate()) {
                        builder.append(value);
                    }
                    String text = builder.toString();
                    System.out.println(text);
                    break;
                case 2:
                    System.out.println("Visitors with more than 2 pizzas:");
                    System.out.println(pizzeria.getAddressesMoreThanTwoPizzas());
                    break;
                case 3:
                    System.out.println("Count orders with this pizza");
                    System.out.println("Choose pizza: ");
                    System.out.println(pizzeria.getAvailablePizzas().stream()
                            .map(Pizza::getName)
                            .collect(Collectors.joining(", ")));
                    int pizzaChoice = getUserChoice();
                    if (pizzaChoice > pizzeria.getAvailablePizzas().size()) {
                        System.out.println("Not valid number");
                    } else {
                        System.out.println(pizzeria.countVisitorsWithSetPizza(pizzeria.getAvailablePizzas().get(pizzaChoice - 1)));
                    }
                    break;
                case 4:
                    System.out.println("The biggest number of same pizzas for chosen visitor");
                    System.out.println("Choose visitor: ");
                    StringBuilder builder2 = new StringBuilder();
                    for (Visitor value : pizzeria.getVisitors()) {
                        builder2.append(value.getDeliveryAddress()).append(", ").append(value.getOrders().size()).append(" orders\n");
                    }
                    String text2 = builder2.toString();
                    System.out.println(text2);
                    int visitorChoice = getUserChoice();
                    if (visitorChoice > pizzeria.getVisitors().size()) {
                        System.out.println("Not valid number");
                    } else {
                        Map<Pizza, Long> res = pizzeria.findMoreCount(pizzeria.getAvailablePizzas(), pizzeria.getVisitors().get(visitorChoice - 1));
                        Map.Entry<Pizza, Long> maxEntry = res.entrySet().stream()
                                .max(Map.Entry.comparingByValue())
                                .orElse(null);
                        if (maxEntry != null) {
                            System.out.println("Pizza with max count: " + maxEntry.getKey() +
                                    " | Count: " + maxEntry.getValue());
                        } else {
                            System.out.println("The map is empty.");
                        }
                        System.out.println();
                    }
                    break;
                case 5:
                    System.out.println("Pizza - order list map:");
                    Map<Pizza, List<Visitor>> pizzasWithVisitors = pizzeria.getPizzasWithVisitors();

                    pizzasWithVisitors.forEach((pizza, visitors) -> {
                        System.out.println("Pizza: " + pizza.getName());
                        System.out.println("Visitors: " + visitors.stream()
                                .distinct()
                                .map(Visitor::getDeliveryAddress)
                                .toList());
                        System.out.println("------------");
                    });

                    break;
                case 6:
                    System.out.println("Pizzeria didn't make those orders at time");
                    System.out.println(pizzeria.getOrdersInPast());
                    break;
                case 7:
                    pizzeria.getVisitors().forEach(visitor -> {
                        visitor.getOrders().forEach(order -> {
                            System.out.println("Visitor: " + visitor.getDeliveryAddress());
                            System.out.println("Order Number: " + order.getOrderNumber());
                            System.out.println("Delivery Address: " + visitor.getDeliveryAddress());
                        });
                    });
                    System.out.println("Choose order: ");
                    int orderChoice = getUserChoice();
                    pizzeria.makeOrderDone(orderChoice);
                    break;
                case 8:
                    System.out.println("Exiting Pizzeria Manager. Goodbye!");
                    pizzeria.serialize("pizzeria.ser");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    private static int getUserChoice() {
        System.out.print("Enter your choice: ");
        while (!scanner.hasNextInt()) {
            System.out.println("Invalid input. Please enter a number.");
            scanner.next();
        }
        return scanner.nextInt();
    }
}