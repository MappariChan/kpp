package org.example;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
@Setter
public class Pizzeria implements Serializable {
    @Serial
    private static final long serialVersionUID = 4589106174685945177L;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private List<Pizza> availablePizzas;
    private List<Visitor> visitors;

    public Pizzeria(List<Pizza> availablePizzas, List<Visitor> visitors) {
        this.availablePizzas = new ArrayList<>(availablePizzas);
        this.visitors = new ArrayList<>(visitors);
    }

    public static Pizzeria deserialize(String fileName) {
        Pizzeria pizzeria = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(fileName))) {
            pizzeria = (Pizzeria) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e);
        }
        return pizzeria;
    }

    public void serialize(String fileName) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(this);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public List<Order> sortOrdersByDate() {
        return visitors.stream()
                .flatMap(visitor -> visitor.getOrders().stream())
                .sorted(Comparator.comparing(Order::getOrderDate))
                .collect(Collectors.toList());
    }

    public List<String> getAddressesMoreThanTwoPizzas() {
        Map<Visitor, List<Order>> userOrdersMap = visitors.stream()
                .collect(Collectors.toMap(visitor -> visitor, Visitor::getOrders));

        return userOrdersMap.entrySet().stream()
                .filter(entry -> entry.getValue().stream().mapToInt(order -> order.getPizzas().size()).sum() > 2)
                .map(entry -> entry.getKey().getDeliveryAddress())
                .distinct()
                .collect(Collectors.toList());
    }

    public long countVisitorsWithSetPizza(Pizza pizza) {
        Stream.of(1, 2, 3);
        return visitors.stream()
                .filter(visitor -> visitor.getOrders().stream()
                        .flatMap(order -> order.getPizzas().stream())
                        .anyMatch(pizza1 -> pizza1.equals(pizza)))
                .count();
    }

    public Map<Pizza, Long> findMoreCount(List<Pizza> pizzas, Visitor visitor) {
        return pizzas.stream()
                .collect(Collectors.toMap(pizza -> pizza, pizza -> visitor.getOrders().stream()
                        .flatMap(order -> order.getPizzas().stream())
                        .filter(pizza1 -> pizza1.equals(pizza))
                        .count()));
    }

    public Map<Pizza, List<Visitor>> getPizzasWithVisitors() {
        return availablePizzas.stream()
                .collect(Collectors.toMap(pizza -> pizza, pizza -> visitors.stream()
                        .filter(visitor -> visitor.getOrders().stream()
                                .flatMap(order -> order.getPizzas().stream())
                                .anyMatch(pizza1 -> pizza1.equals(pizza)))
                        .collect(Collectors.toList())));
    }

    public List<Order> getOrdersInPast() {
        return visitors.stream()
                .flatMap(visitor -> visitor.getOrders().stream())
                .filter(order -> order.getOrderDate().isBefore(LocalDateTime.now()))
                .collect(Collectors.toList());
    }


    public void makeOrderDone(int orderNumber) {
        visitors.forEach(visitor -> visitor.getOrders().removeIf(order -> order.getOrderNumber() == orderNumber));
    }

    @Override
    public String toString() {
        String res = "Available pizzas: ";
        res += (availablePizzas.stream()
                .map(Pizza::toString)
                .collect(Collectors.joining("\n")));
        res += "\n\nVisitors:";
        res += (visitors.stream()
                .map(Visitor::toString)
                .collect(Collectors.joining("\n")));
        return res;
    }
}
