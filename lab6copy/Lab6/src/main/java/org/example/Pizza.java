package org.example;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Pizza implements Serializable {
    @Serial
    private static final long serialVersionUID = -2501434322551626439L;

    private String name;
    private int weight;
    private int price;
    private List<String> ingredients;

    public Pizza(String name, int weight, int price, List<String> ingredients) {
        this.name = name;
        this.weight = weight;
        this.price = price;
        this.ingredients = new ArrayList<>(ingredients);
    }

    public void addIngredient(String ingredient) {
        ingredients.add(ingredient);
    }

    @Override
    public String toString() {
        return String.format("Name: %s, Weight: %d, Price: %d, Ingredients: %s",
                name, weight, price, ingredients);
    }
}
