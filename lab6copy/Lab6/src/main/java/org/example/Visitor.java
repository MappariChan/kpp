package org.example;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Visitor implements Serializable {
    @Serial
    private static final long serialVersionUID = 1842549373559498061L;

    private String deliveryAddress;
    private List<Order> orders;

    public Visitor(String deliveryAddress, List<Order> orders) {
        this.deliveryAddress = deliveryAddress;
        this.orders = new ArrayList<>(orders);
    }

    public void addDelivery(Order order) {
        orders.add(order);
    }

    @Override
    public String toString() {
        return String.format("Delivery address: %s, Orders: %s\n", deliveryAddress, orders);
    }
}
