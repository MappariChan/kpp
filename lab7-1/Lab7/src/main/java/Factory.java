import java.util.Stack;

public class Factory {
    private Stack<Resource> resources;

    public Factory(int amount){
        resources = new Stack<>();
        for(int i = 0; i < amount; i++){
            resources.push(new Resource());
        }
    }

    public synchronized Resource useResource(){
        return resources.pop();
    }

    public synchronized Good produceGood(){
        if (resources.size() > 0) {
            System.out.println(Thread.currentThread().getName() + " obtained a good. Resource left: " + (resources.size() - 1));
            return new Good(useResource());
        } else {
            System.out.println(Thread.currentThread().getName() + " is waiting for a new resources.");
            try {
                wait();
            } catch (InterruptedException ignored) {
            }
        }
        return null;
    }
}