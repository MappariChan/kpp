import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadFactory;

public class ConsumersThreadFactory implements ThreadFactory {
    List<Thread> list;
    int counter;

    public ConsumersThreadFactory(List<Thread> list){this.list = list;counter = 1;}
    public Thread newThread(Runnable r){
        var random = new Random();
        Thread t = new Thread(r,"Consumer " + counter);
        counter++;
        list.add(t);
        t.setPriority(random.nextInt(1,10));
        return t;
    }
}
