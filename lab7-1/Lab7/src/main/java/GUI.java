import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.time.LocalTime;
import java.util.concurrent.*;

public class GUI extends JFrame {
    private final Consumer[] consumers;
    private final List<Thread> threads;
    private final JButton[] startButtons;
    private final JButton[] suspendButtons;
    private final JButton[] stopButtons;
    private final JLabel[] stateLabels;
    private final JLabel[] timeLabels;
    private final boolean[] suspendStatuses;
    private final String[] previousThreadStatuses;

    private final ExecutorService executorService;

    public GUI(int threadCount, int booksLimit) {
        super("Factory");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridLayout(threadCount, 3));
        Factory factory = new Factory(booksLimit);
        consumers = new Consumer[threadCount];
        startButtons = new JButton[threadCount];
        suspendButtons = new JButton[threadCount];
        stopButtons = new JButton[threadCount];
        previousThreadStatuses = new String[threadCount];
        threads = new LinkedList<>();
        suspendStatuses = new boolean[threadCount];
        timeLabels = new JLabel[threadCount];
        JPanel[] infoPanels = new JPanel[threadCount];
        JLabel[] nameLabels = new JLabel[threadCount];
        stateLabels = new JLabel[threadCount];
        JLabel[] priorityLabels = new JLabel[threadCount];
        executorService = Executors.newFixedThreadPool(threadCount, new ConsumersThreadFactory(threads));

        for (int i = 0; i < threadCount; i++) {
            //readers[i] = new Reader(library);
            consumers[i] = new Consumer(factory);
            suspendStatuses[i] = false;
            previousThreadStatuses[i] = "NEW";
            startButtons[i] = new JButton("Start");
            final int finalI = i;
            startButtons[i].addActionListener(e -> {
                //executorService.submit(readers[finalI]);
                executorService.submit(consumers[finalI]);
                nameLabels[finalI].setText("Name: " + threads.get(finalI).getName());
                stateLabels[finalI].setText("State: " + threads.get(finalI).getState());
                priorityLabels[finalI].setText("Priority: " + threads.get(finalI).getPriority());
                timeLabels[finalI].setText("State change time: " + LocalTime.now());
                startButtons[finalI].setEnabled(false);
                Timer timer = new Timer(1000, ex -> {
                    stateLabels[finalI].setText("State: " + threads.get(finalI).getState());
                    if (!Objects.equals(threads.get(finalI).getState().toString(), previousThreadStatuses[finalI])) {
                        previousThreadStatuses[finalI] = threads.get(finalI).getState().toString();
                        timeLabels[finalI].setText("State change time:" + LocalTime.now());
                    }
                });
                timer.start();
            });

            suspendButtons[i] = new JButton("Suspend");
            suspendButtons[i].addActionListener(e -> {
                if (!suspendStatuses[finalI]) {
                    consumers[finalI].suspend();
                    suspendStatuses[finalI] = true;
                    suspendButtons[finalI].setText("Resume");
                } else {
                    consumers[finalI].resume();
                    suspendStatuses[finalI] = false;
                    suspendButtons[finalI].setText("Suspend");
                }
            });

            stopButtons[i] = new JButton("Stop");
            stopButtons[i].addActionListener(e -> {
                consumers[finalI].stop();
                stopButtons[finalI].setEnabled(false);
                suspendButtons[finalI].setEnabled(false);
            });

            infoPanels[i] = new JPanel();
            infoPanels[i].setLayout(new GridLayout(4, 1));
            nameLabels[i] = new JLabel("Name: Undefined");
            stateLabels[i] = new JLabel("State: NEW");
            priorityLabels[i] = new JLabel("Priority: Undefined");
            timeLabels[i] = new JLabel("State change time: " + LocalTime.now());
            infoPanels[i].add(nameLabels[i]);
            infoPanels[i].add(stateLabels[i]);
            infoPanels[i].add(priorityLabels[i]);
            infoPanels[i].add(timeLabels[i]);

            add(startButtons[i]);
            add(suspendButtons[i]);
            add(stopButtons[i]);
            add(infoPanels[i]);
        }

        setVisible(true);
    }
}