import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter threads count:");
        int threadCount = scanner.nextInt();
        System.out.println("Enter resource amount:");
        int resourceAmount = scanner.nextInt();
        GUI gui = new GUI(threadCount, resourceAmount);
    }
}