public class Consumer implements Runnable{
    private final Factory factory;
    private boolean isRunning;
    private boolean isSuspended;

    public Consumer(Factory factory) {
        this.factory = factory;
        isRunning = true;
        isSuspended = false;
    }

    public void run() {
        while (isRunning) {
            synchronized (this) {
                while (isSuspended) {
                    try {
                        wait();
                    } catch (InterruptedException ignored) {
                    }
                }
            }
            factory.produceGood();
            try {
                Thread.sleep((long) (Math.random() * 10000));
            } catch (InterruptedException ignored) {
            }
        }
    }

    public void stop() {
        this.isRunning = false;
    }

    public void suspend() {
        this.isSuspended = true;
    }

    public void resume() {
        this.isSuspended = false;
        synchronized (this) {
            notify();
        }
    }
}
