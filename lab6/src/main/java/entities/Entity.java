package entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Entity implements Serializable {
    private int id;
    private Map<Class<? extends Entity>, Reference> referenceMap;
    private static final long serialVersionUID = 1L;

    public Entity(){
        referenceMap = new HashMap<>();
    }
    public Entity(Map<Class<? extends Entity>, Reference> referenceMap){
        this.referenceMap = referenceMap;
    };
    public Entity(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public Map<Class<? extends Entity>, Reference> getReferenceMap() {
        return referenceMap;
    }

    public void setReferenceMap(Map<Class<? extends Entity>, Reference> referenceMap) {
        this.referenceMap = referenceMap;
    }
}
