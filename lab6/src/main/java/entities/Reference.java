package entities;

import java.io.Serializable;

public class Reference implements Serializable {
    private String reference;
    private String referencedField;

    public Reference(String reference, String referencedField){
        this.reference = reference;
        this.referencedField = referencedField;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReferencedField() {
        return referencedField;
    }

    public void setReferencedField(String referencedId) {
        this.referencedField = referencedId;
    }
}
