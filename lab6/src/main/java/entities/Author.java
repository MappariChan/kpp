package entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Author extends Entity {
    private String fullName;
    private int age;
    private int bookId;
    private Book book;
    private List<Book> books;

    public Author(){
        Map<Class<? extends Entity>, Reference> referenceMap = new HashMap<>();
        referenceMap.put(Book.class, new Reference("bookId", "book"));
        setReferenceMap(referenceMap);
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public Book getBook() {
        return book;
    }
}
