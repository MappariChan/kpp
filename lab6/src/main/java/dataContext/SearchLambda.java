package dataContext;

@FunctionalInterface
public interface SearchLambda<T, R> {
    R search(T t);
}
