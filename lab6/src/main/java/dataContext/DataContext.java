package dataContext;

import entities.*;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class DataContext {
    private static DataContext dataContext;
    private DataSet<Author> authors;
    private DataSet<Book> books;
    private DataSet<User> users;
    private DataSet<BorrowRecord> borrowRecords;

    public DataSet<Author> getAuthors() {
        return authors;
    }

    public DataSet<Book> getBooks() {
        return books;
    }

    public DataSet<User> getUsers() {
        return users;
    }

    public DataSet<BorrowRecord> getBorrowRecords() {
        return borrowRecords;
    }

    private DataContext(){
        authors = new DataSet<>("authors.ser");
        books = new DataSet<>("books.ser");
        users = new DataSet<>("users.ser");
        borrowRecords = new DataSet<>("borrowRecords.ser");
    }

    public static DataContext getInstance(){
        if(dataContext == null){
            dataContext = new DataContext();
            return dataContext;
        }
        return dataContext;
    }

    public <T extends Entity> DataSet<T> GetSet(Class<T> clazz) {
        try {
            Field dataSetField = getClass().getDeclaredField(clazz.getSimpleName().toLowerCase() + "s");
            ParameterizedType genericType = (ParameterizedType) dataSetField.getGenericType();
            Type[] typeArguments = genericType.getActualTypeArguments();
            if (typeArguments.length > 0) {
                @SuppressWarnings("unchecked")
                Class<?> dataSetType = (Class<?>) typeArguments[0];
                if (dataSetType.isAssignableFrom(clazz)) {
                    return (DataSet<T>) dataSetField.get(this);
                }
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace(); // Handle or log the exception as needed
        }
        throw new IllegalArgumentException("Unsupported class type: " + clazz.getName());
    }
}
