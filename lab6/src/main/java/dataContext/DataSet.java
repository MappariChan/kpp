package dataContext;

import entities.Entity;
import entities.Reference;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataSet<T extends Entity> {
    private String path;
    private int currentId;

    public DataSet(String path){
        this.path = path;
        List<T> entities = GetAll().collect(Collectors.toList());
        currentId = entities.size() != 0 ? entities.getLast().getId() + 1 : 1;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Stream<T> GetAll(){
        ArrayList<T> list = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))) {
            // Read objects until the end of the file
            while (true) {
                try {
                    T loadedObj = (T) ois.readObject();
                    list.add(loadedObj);
                } catch (EOFException e) {
                    // End of file reached
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list.stream();
    }

    public T Get(SearchLambda<T, Boolean> searchLambda){
        Stream<T> all = GetAll();
        return all.filter(item -> searchLambda.search(item)).findFirst().get();
    }

    public void Create(T itemToCreate){
        List<T> allItems = GetAll().collect(Collectors.toList());
        itemToCreate.setId(currentId);
        allItems.add(itemToCreate);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            for(T item : allItems) {
                oos.writeObject(item);
            }
            currentId += 1;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Delete(int id){
        Stream<T> all = GetAll();
        List<T> filteredList = all.filter(t -> t.getId() != id).collect(Collectors.toList());
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            for(T item : filteredList) {
                oos.writeObject(item);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<T> AssociateCollectionWith(Class<? extends Entity>... Classes){
        List<T> allItems = GetAll().collect(Collectors.toList());
        for(T item : allItems){
            for(Class<? extends Entity> Class : Classes){
                Reference reference = item.getReferenceMap().get(Class);
                Class<? extends Entity> itemClass = item.getClass();
                try{
                    Field field = itemClass.getDeclaredField(reference.getReference());
                    field.setAccessible(true);
                    int referenceId = (int)field.get(item);
                    Field referencedField = itemClass.getDeclaredField(reference.getReferencedField());
                    referencedField.setAccessible(true);
                    DataSet<? extends Entity> set = DataContext.getInstance().GetSet(Class);
                    List<? extends Entity> associatedItems = set.GetAll().collect(Collectors.toList());
                    for(var associatedItem : associatedItems){
                        if(referenceId == associatedItem.getId()){
                            referencedField.set(item, associatedItem);
                        }
                    }
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return allItems;
    }
}
