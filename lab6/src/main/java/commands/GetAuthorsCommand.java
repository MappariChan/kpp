package commands;

import dataContext.DataContext;
import dataContext.DataSet;
import entities.Author;
import entities.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GetAuthorsCommand implements Command{
    @Override
    public void execute() {
        DataSet<Author> authors = DataContext.getInstance().GetSet(Author.class);

        List<Author> allAuthors = authors.AssociateCollectionWith(Book.class);

        for(Author author : allAuthors){
            System.out.println(author.getFullName() + " " + author.getAge() + " " + (author.getBook() != null ? author.getBook().getName():"does not have any book") + " " + author.getId());
        }
    }
}
