package commands;

import dataContext.DataContext;
import dataContext.DataSet;
import entities.Author;

import java.util.ArrayList;
import java.util.Scanner;

public class AddAuthorCommand implements Command{
    @Override
    public void execute() {
        DataSet<Author> authors = DataContext.getInstance().GetSet(Author.class);
        Scanner scanner = new Scanner(System.in);

        Author author = new Author();

        System.out.println("Enter author fullName:");
        String fullName = scanner.nextLine();
        System.out.println("Enter author age");
        int age = scanner.nextInt();

        author.setFullName(fullName);
        author.setAge(age);
        author.setBooks(new ArrayList<>());

        authors.Create(author);
    }
}
