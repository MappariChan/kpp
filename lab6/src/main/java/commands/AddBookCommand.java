package commands;

import dataContext.DataContext;
import dataContext.DataSet;
import entities.Author;
import entities.Book;
import entities.BorrowRecord;

import java.util.ArrayList;
import java.util.Scanner;

public class AddBookCommand implements Command{
    @Override
    public void execute() {
        DataSet<Book> books = DataContext.getInstance().GetSet(Book.class);
        Scanner scanner = new Scanner(System.in);

        Book book = new Book();

        System.out.println("Enter book name:");
        String name = scanner.nextLine();
        System.out.println("Enter author id");
        int authorId = scanner.nextInt();

        book.setName(name);
        book.setAuthors(new ArrayList<Author>());
        book.setAvailable(true);
        book.setBorrowHistory(new ArrayList<BorrowRecord>());

        books.Create(book);

        DataSet<Author> authors = DataContext.getInstance().GetSet(Author.class);
        Author author = authors.Get(searchedAuthor -> searchedAuthor.getId() == authorId);
        authors.Delete(author.getId());
        author.setBookId(book.getId());
        authors.Create(author);
    }
}
