package org.example;

import commands.AddAuthorCommand;
import commands.AddBookCommand;
import commands.Command;
import commands.GetAuthorsCommand;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Command command = null;
        boolean isRunning = true;

        while(true) {
            String commandString = scanner.nextLine();
            switch (commandString) {
                case "add-author":
                    command = new AddAuthorCommand();
                    break;
                case "get-authors":
                    command = new GetAuthorsCommand();
                    break;
                case "add-book":
                    command = new AddBookCommand();
                    break;
                default:
                    isRunning = false;
            }
            if(!isRunning){
                break;
            }
            command.execute();
        }
    }
}